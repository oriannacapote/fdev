import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-section-image',
  templateUrl: './section-image.component.html',
  styleUrls: ['./section-image.component.css']
})
export class SectionImageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  imageChangedEvent: any = '';
    croppedImage: any = '';
    
    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
        console.log("----------------------------------", event)
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        event.height = 800;
        console.log( "aaaaaaaaaaaaaaaaaaaaaaaaaaaa", event);
    }
    imageLoaded() {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }

}
