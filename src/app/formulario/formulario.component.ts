import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

interface Country {
  value: string;
  viewValue: string;
}

export interface PeriodicElement {
  symbol: string;
  name: string;
  position: string;
  weight: number;
  a: string;
  b: string;
  d: string;
  c: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 'Lunes', name: 'Hydrogen', weight: 1.0079, symbol: 'Abierto 24hrs', a: 'a', b: 'b', c: 'c', d:'d'},
  {position: 'Martes', name: 'Helium', weight: 4.0026, symbol: 'Cerrado', a: 'a', b: 'b', c: 'c', d:'d'},
  {position: 'Miercoles', name: 'Lithium', weight: 6.941, symbol: 'Horario', a: 'a', b: 'b', c: 'c', d:'d'}
];

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  countrys: Country[] = [
    {value: '0', viewValue: 'Colombia'},
    {value: '1', viewValue: 'Mexico'},
    {value: '2', viewValue: 'Venezuela'}
  ];
  displayedColumns = [ 'symbol', 'position', 'name', 'weight', 'a', 'b', 'c', 'd'];
  dataSource = ELEMENT_DATA;

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

}